import Vue from 'vue';
import App from './App.vue';
import store from './store';
import './components';

import './assets/sass/index.scss';
import './boot/filters';

Vue.config.productionTip = false;

new Vue({
  store,
  render: (h) => h(App),
}).$mount('#app');
