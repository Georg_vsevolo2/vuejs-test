import Vue from 'vue';

Vue.filter('input', (value) => {
  if (value) {
    const price = parseFloat(value.replace(/\s+/g, ''));
    return price.toLocaleString('ru-RU', { maximumFractionDigits: 2 });
  }
  return null;
});
Vue.filter('price', (value) => {
  if (value) {
    return value.toLocaleString('ru-RU', { maximumFractionDigits: 2 });
  }
  return null;
});
Vue.filter('date', (value) => {
  const date = new Date(value);
  return new Intl.DateTimeFormat('ru-RU', { day: 'numeric', month: 'numeric', year: 'numeric' }).format(date);
});
