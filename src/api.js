import axios from 'axios';

/**
 * @var {Axios}
 */
const instance = axios.create({
  baseURL: process.env.VUE_APP_API_URL,
});

/**
 * Load payments data.
 *
 * @param {Object} params
 * @returns {Promise}
 */
function getPayments(params = {}) {
  if (1) {
    // const mocks = import('/src/mocks/getPayments');
    const mocks = new Promise((resolve) => {
      setTimeout(() => {
        resolve({
          data: [
            {
              id: 1,
              name: 'Bob Lee',
              money: 1000,
              date: '2020-02-24',
            },
            {
              id: 2,
              name: 'John Seek',
              money: 1999.99,
              date: '2020-02-25',
            },
            {
              id: 3,
              name: 'Harry Smith',
              money: 3000.5,
              date: '2020-02-26',
            },
            {
              id: 4,
              name: 'Alex Morphy',
              money: 4000,
              date: '2020-02-27',
            },
            {
              id: 5,
              name: 'Ben Gold',
              money: 5000,
              date: '2020-02-28',
            },
            {
              id: 6,
              name: 'Tim Black',
              money: 10000,
              date: '2020-02-28',
            },
            {
              id: 7,
              name: 'Jimmy Elephant',
              money: 1000000,
              date: '2020-02-30',
            },
          ],
        });
      }, 1500);
    });
    // console.log('mocks2', Promise.resolve(mocks));
    // // return Promise.resolve(mocks);
    // Promise.resolve(mocks).then((value) => {
    //   console.log('11', value); // "Success"
    // }, (value) => {
    //   console.log(false); // "Success"
    //   // не будет вызвана
    // });
    return mocks;
  }
  console.log('process', process);

  return () => instance.request({
    method: 'get',
    url: axios.defaults.baseURL,
    params,
  });
}

export default {
  instance,
  getPayments,
};
